# README #

### Localhost to IP Converter ###

* Allow other members of your team to preview local installations of WordPress from their own computer (assuming you are on the same network).

WordPress is a great web application and we LOVE it at Secret Source. Normally, we develop themes and plugins on our local machines. Every project has its own WordPress installation and database. WordPress is installed against 'localhost' and accessed as http://localhost/~myusername/projectname.

This configuration, however, makes it difficult for other members of the team to see our work without getting up out of their chairs and walking over and looking over our shoulders or with the developer pushing the code to a development server. Both options require a little too much effort (or may be impossible if the reviewer is not in the same geographical location).

This plugin makes it possible for other people on your subnet (network) to view your work by accessing your computer via its IP address. So, instead of accessing it via http://localhost/~myusername/projectname you would send the link http://192.168.1.201/~myusername/projectname (substituting your own IP address and path, of course).

### How do I get set up? ###

* Uncompress the zip file and copy it into your plugins folder
* Activate the plugin in the Administration panel

### Contribution guidelines ###

* Make it better (faster, less code, etc.)

### Who do I talk to? ###

* Ted Stresen-Reuter, ted@secret-source.eu