<?php  
/* 
	Plugin Name: Secret Source Localhost to IP Converter 
	Plugin URI: http://www.secret-source.eu
	Description: Converts references to localhost to the server IP address to facilitate peer review in development environments.
	Author: Ted Stresen-Reuter
	Version: 1.0
	Author URI: http://chicagoitsystems.com
*/
/*  Copyright 2014  Ted Stresen-Reuter
	
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

function ss_localhost_to_ip ($old_url) {
	if(preg_match('/^[0-9]{3,}/', $_SERVER['HTTP_HOST'])) {
		$ss_hostname = $_SERVER['HTTP_HOST'];
		$ss_o = parse_url($old_url);
		$ss_new_url = $ss_o['scheme'] . '://' . $ss_hostname . $ss_o['path'];
		if ($ss_o['query'] != '') {
			$ss_new_url .= '?' . $ss_o['query'];
		}
		if ($ss_o['fragment'] != '') {
			$ss_new_url .= '#' . $ss_o['fragment'];
		}
	} else {
		$ss_new_url = $old_url;
	}
	return $ss_new_url;
}

add_filter('content_url', 'ss_localhost_to_ip', 10);
apply_filters('content_url', content_url());
add_filter('page_link', 'ss_localhost_to_ip', 10);
apply_filters('page_link', content_url());
add_filter('post_link', 'ss_localhost_to_ip', 10);
apply_filters('post_link', content_url());
add_filter('tag_link', 'ss_localhost_to_ip', 10);
apply_filters('tag_link', content_url());
add_filter('category_link', 'ss_localhost_to_ip', 10);
apply_filters('category_link', content_url());
add_filter('author_link', 'ss_localhost_to_ip', 10);
apply_filters('author_link', content_url());
add_filter('comment_link', 'ss_localhost_to_ip', 10);
apply_filters('comment_link', content_url());
add_filter('day_link', 'ss_localhost_to_ip', 10);
apply_filters('day_link', content_url());
add_filter('feed_link', 'ss_localhost_to_ip', 10);
apply_filters('feed_link', content_url());
add_filter('home_url', 'ss_localhost_to_ip', 10);
apply_filters('home_url', content_url());

function ss_localhost_to_ip_swapper_action () {
	$c = ob_get_clean();
	if(preg_match('/^[0-9]{3,}/', $_SERVER['HTTP_HOST'])) {
		$ip = $_SERVER['HTTP_HOST'];
		$ss_start_buffer = preg_replace('/(src|href)\s*=\s*(\'|")http(s)?:\/\/(localhost)/i', "$1=$2http$3://$ip", $c);
		ob_start();
	} else {
		$ss_start_buffer = $c;
	}
	echo $ss_start_buffer;
}

add_action('init', function(){ ob_start(); });
add_action('wp_loaded', 'ss_localhost_to_ip_swapper_action', 1000);
add_action('wp_head', 'ss_localhost_to_ip_swapper_action', 1000);
add_action('wp_footer', 'ss_localhost_to_ip_swapper_action', 1000);
add_action('wp_print_footer_scripts', 'ss_localhost_to_ip_swapper_action', 1000);
add_action('shutdown', function(){ echo ob_get_clean(); }, 1000);